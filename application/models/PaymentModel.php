<?php
class PaymentModel extends CI_Model {

    public function getPaymentProof($user_id) {
        // Fetch payment proof information from the database based on user_id
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('payment_proof'); // 'payment_proof' should be your table name

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
}
