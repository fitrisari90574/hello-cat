<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class m_artikel extends CI_Model
{
    public function get_polibyid($id){
        return $this->db->get_where('artikel',array('id_artikel'=>$id))->result_array();
    }
}