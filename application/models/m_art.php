<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_Art extends CI_Model
{
    public function get_dataart($id){
        return $this->db->get_where('artikel',['id_artikel'=>$id])->row_array();
    }
    public function searchart($keyword){
        $this->db->like('kategori_artikel', $keyword);
        $this->db->or_like('judul_artikel', $keyword);
        $this->db->or_like('konten_artikel',$keyword);
        $this->db->or_like('sumber_artikel',$keyword);
        return $this->db->get('artikel')->result_array();
    }
}