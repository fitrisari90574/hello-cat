<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Janji extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_user');
        $this->load->model('m_chatbot');
        $this->load->model('m_rs');
    }
    public function index(){
        $this->m_user->checklogin();
        $data['title'] = "Hellocat | Login Chat";
        $id = $this->session->all_userdata();
        $data['chatbot'] = $this->m_chat->getjanji($id['user']['id']);
        $this->load->view('header_page',$data);
        $this->load->view('v_loginchat',$data);
        $this->load->view('footer_page');
    }
    public function update($id){
        $nama = $this->input->post('ubah-nama');
        $emall = $this->input->post('ubah-email');
        if($nama && $email){
            $data =[
                "nama_user"=>$nama,
                "email_user"=>$email
            ];
        }else if($nama){
            $data = [
                "nama_user"=>$nama
            ];
        }else if($email){
            $data = [
                "email_user"=>$email
            ];
        }
        
        $this->db->where("id_janji",$id);
        $this->db->update('janji',$data);
        redirect('janji/','refresh');
    }
    public function hapus($id){
        $this->db->where("id_janji",$id);
        $this->db->delete('janji');
        redirect('janji/','refresh');
    }
}
