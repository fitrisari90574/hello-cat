<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Art extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_art');
        $this->load->model('m_rs');
        $this->load->model('m_user');
        $this->load->model('m_dokter');
        $this->load->model('m_poli');
    }
    public function index(){
        $this->m_user->checklogin();
        $data['title'] = "HELLOCAT | Artikel";
        if($this->input->post('search')){
            $data['artikel'] = $this->m_art->searchart($this->input->post('search'));
        }else{
            $data['artikel'] = $this->db->get('artikel')->result_array();
        }
        
        $this->load->view('header_page',$data);
        $this->load->view('v_art',$data);
        $this->load->view('footer_page');
    }
    public function DetailART($id){
        $this->m_user->checklogin();
        $data['title'] = "HELLOCAT | Detail ARTIKEL";
        $data['artid'] = $this->m_art->get_dataart($id);
        $data['rsid'] = $this->m_rs->get_datars($id);
        $data['drrs'] = $this->m_dokter->get_dokterbyid($id);
        $data['poli'] = $this->m_poli->get_polibyid($id);
        $this->load->view('header_page',$data);
        $this->load->view('v_detailart',$data);
        $this->load->view('footer_page');
    }

    
}
