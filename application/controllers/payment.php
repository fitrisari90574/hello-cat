<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Load the necessary models and libraries here
        $this->load->model('PaymentModel'); // Adjust the model name as per your project
    }

    public function viewPaymentProof($user_id) {
        // Fetch payment proof information from the database based on the user_id
        $payment_proof = $this->PaymentModel->getPaymentProof($user_id);

        if ($payment_proof) {
            $data['payment_proof'] = $payment_proof;
            $this->load->view('payment_proof_view', $data); // Create a view file to display the payment proof
        } else {
            // Handle if the payment proof doesn't exist or user_id is not found
            // You can redirect to an error page or show a message
        }
    }
}
