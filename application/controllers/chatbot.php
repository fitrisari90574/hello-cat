<?php

// Import the database connection class
require_once 'Database.php';

// Create a new database connection object
$database = new Database();

// Define the controller methods
class ChatbotController
{
    public function index()
    {
        // Get all chatbot messages
        $messages = $database->query('SELECT * FROM chatbot');

        // Return the messages
        return $messages;
    }

    public function create()
    {
        // Get the request data
        $data = $this->getRequestData();

        // Validate the request data
        $this->validateRequestData($data);

        // Save the chatbot message
        $database->query('INSERT INTO chatbot (time, pesan, dokterID, userID, fotoUs) VALUES (?, ?, ?, ?, ?)', $data);

        // Return the success message
        return ['message' => 'Chatbot message created successfully'];
    }

    public function update()
    {
        // Get the request data
        $data = $this->getRequestData();

        // Validate the request data
        $this->validateRequestData($data);

        // Update the chatbot message
        $database->query('UPDATE chatbot SET time = ?, pesan = ?, dokterID = ?, userID = ?, fotoUs = ? WHERE chatbotID = ?', $data);

        // Return the success message
        return ['message' => 'Chatbot message updated successfully'];
    }

    public function delete()
    {
        // Get the request data
        $data = $this->getRequestData();

        // Validate the request data
        $this->validateRequestData($data);

        // Delete the chatbot message
        $database->query('DELETE FROM chatbot WHERE chatbotID = ?', $data);

        // Return the success message
        return ['message' => 'Chatbot message deleted successfully'];
    }

    private function getRequestData()
    {
        // Get the request data from the POST or GET request
        $data = $_POST ?? $_GET;

        // Validate the request data
        if (!isset($data['time'])) {
            throw new Exception('Time is required');
        }

        if (!isset($data['pesan'])) {
            throw new Exception('Pesan is required');
        }

        if (!isset($data['dokterID'])) {
            throw new Exception('DokterID is required');
        }

        if (!isset($data['userID'])) {
            throw new Exception('UserID is required');
        }

        if (!isset($data['fotoUs'])) {
            throw new Exception('FotoUs is required');
        }

        // Return the request data
        return $data;
    }

    private function validateRequestData(<span class="math-inline">data\)
\{
// Validate the time
if \(\!preg\_match\('/^\[0\-9\]\{1,6\}</span>/', <span class="math-inline">data\['time'\]\)\) \{
throw new Exception\('Time must be a valid time'\);
\}}
// Validate the pesan
if \(\!preg\_match\)('/^\[a\-zA\-Z0\-9 ,\.\!?\\'\\)";\:\-\_\\n\\r\]\{0,1000\}</span>/', <span class="math-inline">data\['pesan'\]\)\) \{
throw new Exception\('Pesan must be a valid string'\);
\}
// Validate the dokterID
if \(\!preg\_match\('/^\[0\-9\]\{1,10\}</span>/', <span class="math-inline">data\['dokterID'\]\)\) \{
throw new Exception\('DokterID must be a valid integer'\);
\}
// Validate the userID
if \(\!preg\_match\('/^\[0\-9\]\{1,100\}</span>/', <span class="math-inline">data\['userID'\]\)\) \{
throw new Exception\('UserID must be a valid integer'\);
\}
// Validate the fotoUs
if \(\!preg\_match\('/^\[a\-zA\-Z0\-9 ,\.\!?\\'\\";[\:\-\_\\n\\r\]\{0,1000\}</span>/', $data['fotoUs'])) {
            throw new Exception('FotoUs must be a valid string');
        }
    }
}
