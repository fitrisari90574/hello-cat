<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bukti Transaksi</title>

    <style>
        body {
            font-family: sans-serif;
            margin: 0;
            padding: 0;
           

        }

        .container {
            background-image: url('https://i.postimg.cc/sXnMFKKh/bgh.png');
            width: 350px;
            margin: 0 auto;
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 20px;
            background-color: #f5f5f5;
        }

        .header .store-logo {
            width: 100px;
            height: auto;
        }

        .header .store-name {
            font-size: 20px;
            font-weight: bold;
        }

        .order-details {
            padding: 20px;
            border: 1px solid #ccc;
        }
        .detail-link {
            color: red;
            margin-left: 10px;
        }
        .quantity {
            margin-left: 10px;
        }

        .quantity button {
            padding: 5px 10px;
            margin: 0 5px;
            border: 1px solid #ccc;
            background-color: #f5f5f5;
            cursor: pointer;
            margin-left: 20px;
        }


        .order-details h3 {
            font-size: 18px;
            margin-bottom: 10px;
        }
        .price {
            margin-left: 70px;
        }

        .order-details table {
            width: 100%;
            border-collapse: collapse;
            font-size: 14px;
        }

        .order-details table th,
        .order-details table td {
            padding: 5px;
            border: 1px solid #ccc;
            text-align: left;
        }

        .order-summary {
            padding: 20px;
            border: 1px solid #ccc;
            background-color: rgba(223, 139, 40, 0.3)
        }

        .order-summary h4 {
            font-size: 16px;
            margin-bottom: 10px;
        }

        .order-summary table {
            width: 100%;
            border-collapse: collapse;
            font-size: 14px;
        }

        .order-summary table th,
        .order-summary table td {
            padding: 5px;
            border: 1px solid #ccc;
            text-align: right;
        }

        .order-summary table .total {
            font-weight: bold;
        }
        .checklist {
            color: green;
            margin-left: 200px;
            
        }
        .payment-section {
            display: flex;
            align-items: center;
            margin-top: 50px;
        }

        .pay-button {
            margin-left: 100px;
            background-color: red;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        .nominal {
            background-color: #E29685;
            width: 300px;
            height: 40px;
            border: 2px solid black;
            padding: 20px;
        }

        .footer {
            padding: 20px;
            background-color: #f5f5f5;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <img src="https://i.postimg.cc/BQDjBdfK/removal-1-2x.png" alt="Store Logo" class="store-logo">
        <span class="store-name">Bukti Transaksi</span>
    </div>
    <div class="order-summary" style="text-align: center;">
    <span class="checklist" style="font-size: 24px;">&#10004;</span>
    <h4>Transaksi Berhasil </h4>
    <p>26 Oktober 2023</p>
    <p>14.00 WIB</p>
</div>
    <div class="order-details">
    <h3>Detail Pembayaran</h3>
    <p>No. Referensi <span class="price">417158680856</span></p>
    <p>Sumber Dana <span class="price">Rahmawati</span></p>
    <p>Nama Dokter <span class="price">Drh. Leviosa. SKH.</span></p>
    <p>Jenis Transaksi <span class="price">1 Sesi (1x24 Jam)</span></p>
    
</div>


    <div class="nominal">
    <p><strong>Nominal<span class="price">Rp 50.000</span></strong></p>
</div>

    <div class="payment-section">
    <p><strong>Rp 50.000</strong></p> <p>HelloCat</p>
    <button class="pay-button">Bayar</button>
</div>
   

    
