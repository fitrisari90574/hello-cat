<?php

// Koneksi ke database
$conn = mysqli_connect("localhost", "root", "", "belajar-php");

// Query data dari database
$query = "SELECT * FROM produk";
$result = mysqli_query($conn, $query);

// Buat array untuk menyimpan data produk
$produk = array();
while ($row = mysqli_fetch_assoc($result)) {
    $produk[] = $row;
}

// Buat HTML untuk menampilkan data produk
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Belajar PHP</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<div class="container">

    <h1>Daftar Produk</h1>

    <table class="table">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama Produk</th>
                <th>Harga</th>
                <th>Stok</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($produk as $item) : ?>
                <tr>
                    <td><?= $item['id'] ?></td>
                    <td><?= $item['nama'] ?></td>
                    <td><?= $item['harga'] ?></td>
                    <td><?= $item['stok'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>

</body>
</html>