<style>
  body{
		background-image:  url("<?php echo base_url(); ?>/Assets/bgh.png");
		}
    .col-md-6 {
    background-color: #FFDEAD; 
    padding: 20px; 
  }
  .buton:hover{
    background-color: btn-outline;
  }
  a.artikel{
    text-decoration: none;
    color: black;
    float: right;
  }
  a.artikel:hover{
    color: #ec4638;
  }

  .book-an-appointment-frame {
  margin: 0 auto;
  margin-top: 40px;
}

</style>
<div class="container" style="margin-top: 90px">
  <center>
<table style="width:100%;">
  <tr>
      <td> <a href="<?= base_url('home/index/corona')?>" style="font-size: 150%;text-decoration:none;"><button class="btn btn-outline-info buton" style="border-radius:25px;width:9em;">Corona</button></a> </td>
      <td> <a href="<?= base_url('home/index/vitamin')?>" style="font-size: 150%;text-decoration:none;"><button class="btn btn-outline-info buton" style="border-radius:25px;width:9em;">Vitamin</button></a> </td>
      <td> <a href="<?= base_url('home/index/obat')?>" style="font-size: 150%;text-decoration:none;"><button class="btn btn-outline-info buton" style="border-radius:25px;width:9em;">Obat</button></a> </td>
      <td> <a href="<?= base_url('home/index/virus')?>" style="font-size: 150%;text-decoration:none;"><button class="btn btn-outline-info buton" style="border-radius:25px;width:9em;">Virus</button></a> </td>
      <td> <a href="<?= base_url('home/index/kesehatan')?>" style="font-size: 150%;text-decoration:none;"><button class="btn btn-outline-info buton" style="border-radius:25px;width:9em;">Kesehatan</button></a> </td>
      <td> <a href="<?= base_url('home/index/jantung')?>" style="font-size: 150%;text-decoration:none;"><button class="btn btn-outline-info buton" style="border-radius:25px;width:9em;">Jantung</button></a> </td>
  </tr>
</table>

</div>


<div class="container" style="margin-top: 20px; padding: 10px;">
  <div class="row">
    <div class="col-md-6">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="z-index: 5;">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" style="width: 100%;">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?php echo base_url(); ?>Assets/health.png" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?php echo base_url(); ?>Assets/viruz.png" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?php echo base_url(); ?>Assets/health.png" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    <div class="col-md-6">
      <div class="mask-group1">
        <div class="mask-group-item"></div>
        <div class="rectangle-parent6">
          <div class="group-child19"></div>
          <div class="treating-all-skin-co-group">
          <img src="<?php echo base_url('assets/hello.png'); ?>" alt="Image of a cat" style="width: 200px; height: 50px; margin-bottom: 20px;">
            <div class="treating-all-skin2">
              Konsultasikan kucing kesayangan Anda pada Kami
            </div>
            <div class="book-an-appointment-wrapper1">
              <div class="book-an-appointment3">Welcome!</div>
            </div>
            <div class="book-an-appointment-frame" id="frameContainer1">
            <a href="<?php echo base_url('user/login_chat'); ?>"><button style="background-color: #B88E2F; color: white; padding: 10px 20px; border: none; cursor: pointer;">LOGIN</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main">
  <center>
<div class="container konten">
<h1>ARTIKEL</h1>
<h5 style="font-weight: normal;">Your Daily Informations About Your Lovely Cat!</h5>
</center>
<div class="container" style="margin-top: 10px;margin-bottom:150px;">
    <section class="cards">
    <div class="card" style="width: 18rem;">
      <a href="<?= base_url('janji/')?>"></a>
      <center>
      <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/ar2.png" alt="Card image cap">
    </center>
      <div class="card-body">
        <p> <strong>Waspada! Penyakit Kucing Yang Sering Dianggap Sepele </strong></p>
         <!-- <p class="card-text">Penyakit Kucing Yang Sering Dianggap Sepele.</p>  -->
      </div>
</div>

<div class="card" style="width: 18rem;">
  <a href="<?= base_url('caridokter/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/medical.png" alt="Card image cap">
  </center>
  <div class="card-body">
    <p> <strong>Tips Merawat Kucing Kesayangan Anda</strong> </p>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <a href="<?= base_url('rumahsakit/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/rs.png" alt="Card image cap">
</center>
  <div class="card-body">
    <p> <strong>Cari Tahu Ras Kucing Anda</strong></p>
  </div>
</div>
</section>
</div>

<div class="main">
<div class="container konten">
  <center>
<h1>Dokter</h1>
</center>

<div class="container" style="margin-top: 10px;margin-bottom:150px;">
    <section class="cards">
    <div class="card" style="width: 18rem;">
      <a href="<?= base_url('caridokter/')?>"></a>
      <center>
      <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/pita.png" alt="Card image cap">
    </center>
      <div class="card-body">
        <p> <strong>drh. Fitri Nov. SKH.</strong></p>
        <p class="card-text">STMIK El Rahma YK</p>
        <p> <strong>Rp 50.000</strong></p>
      </div>
</div>

<div class="card" style="width: 18rem;">
  <a href="<?= base_url('caridokter/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/acha.png" alt="Card image cap">
  </center>
  <div class="card-body">
    <p> <strong>drh. Lolito. SKH.</strong> </p>
    <p class="card-text">STMIK El Rahma YK</p>
    <p> <strong>Rp 7.000.000</strong> </p>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <a href="<?= base_url('rumahsakit/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/yoli.png" alt="Card image cap">
</center>
  <div class="card-body">
    <p> <strong>drh. Grifo. SKH.</strong></p>
    <p class="card-text">STMIK El Rahma YK</p>
    <p> <strong>Rp 150.000</strong> </p>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <a href="<?= base_url('rumahsakit/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/acha.png" alt="Card image cap">
</center>
  <div class="card-body">
    <p> <strong>drh. Respira. SKH.</strong></p>
    <p class="card-text">STMIK El Rahma YK</p>
    <p> <strong>Rp 500.000</strong> </p>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <a href="<?= base_url('rumahsakit/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/pita.png" alt="Card image cap">
</center>
  <div class="card-body">
    <p> <strong>drh. Leviosa. SKH.</strong></p>
    <p class="card-text">STMIK El Rahma YK</p>
    <p> <strong>Rp 50.000</strong> </p>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <a href="<?= base_url('rumahsakit/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/yoli.png" alt="Card image cap">
</center>
  <div class="card-body">
    <p> <strong>drh. Muggo. SKH.</strong></p>
    <p class="card-text">STMIK El Rahma YK</p>
    <p> <strong>Rp 150.000</strong> </p>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <a href="<?= base_url('rumahsakit/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/pita.png" alt="Card image cap">
</center>
  <div class="card-body">
    <p> <strong>drh. Pingky. SKH.</strong></p>
    <p class="card-text">STMIK El Rahma YK</p>
    <p> <strong>Rp 7.000.000</strong> </p>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <a href="<?= base_url('rumahsakit/')?>"></a>
  <center>
  <img class="card-img-top" src="<?php echo base_url(); ?>/Assets/pita.png" alt="Card image cap">
</center>
  <div class="card-body">
    <p> <strong>drh. Potty. SKH.</strong></p>
    <p class="card-text">STMIK El Rahma YK</p>
    <p> <strong>Rp 500.000</strong> </p>
  </div>
</div>
<div class="book-an-appointment-frame" id="frameContainer1">
  <button class="primary" style="width: 200px; border-color: yellow;">Show More</button>
</div>
</section>
</div>
<?php if ($artikel){
foreach($artikel as $ar){?> 
<hr>
   <div class="text-container">
      <h3 class="artikel"><?= $ar['judul_artikel']?></h3>
      <?php
      $split = explode('.',$ar['konten_artikel'])
      ?>
      <p class="artikel"><?= $split[0].".".$split[1]."..." ?></p>
      <a href="<?= base_url('home/artikel/'.$ar['id_artikel'])?>" class="artikel" target="_blank">Read More</a>
   </div>
   <br>
<hr>
<?php }?>
<?php }else{ ?>
  <hr>
   <div class="text-container">
      <h3 class="artikel">Artikel Belum Tersedia</h3>
   </div>
   <br>
<hr>
<?php }?>
</div>
</div>

<div class="container" style="margin-top: 20px; padding: 10px;">
  <div class="row">
    <!-- Text and Buttons -->
    <div class="col-md-6">
      <div class="text-container">
        <h2>10+ Jenis Kucing</h2>
        <p>Some description text goes here.</p>
        <div class="button-container">
          <button class="btn btn-primary">Button 1</button>
        </div>
      </div>
    </div>

    <!-- Carousel -->
    <div class="col-md-6">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="z-index: 5;">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?php echo base_url(); ?>Assets/hewan1.png" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?php echo base_url(); ?>Assets/hewan2.png" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?php echo base_url(); ?>Assets/hewan3.png" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>

