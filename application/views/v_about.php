<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.card-about {
  margin-top: 90px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
  margin-left: auto;
  margin-right: 20px;
}
.bodi{
  margin-top: 90px;
  margin-left: 20px;
}
.title-card {
  color: grey;
  font-size: 18px;
}

.imgabt{
  border-radius: 25px 25px 0 0;
  max-width: 100%;
  height: auto;
  object-fit: contain;
}
.about{
  margin-bottom: 40px;
}
.flex-container{
  display: flex;
  margin: auto;
}
.container {
  width: 100%;
  max-width: 960px;
  margin: 0 auto;
}

.row {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}

.col-sm-6 {
  width: 50%;
  padding: 15px;
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

.img-fluid {
  max-width: 100%;
  height: auto;
}

iframe {
  margin-top: 20px;
}

</style>
</head>
<body>
<div class="bodi">
<h2 class="about" style="text-align:left ">Tentang Hellocat</h2>
<p>Web HelloCat merupakan website kesehatan yang dapat menunjang dan mempermudah masyarakat dalam berkonsultasi mengenai kucing melalui chatbot.</p>
<br>
<h2 class="about" style="text-align:center">Para Pendekar HelloCat</h2>
  <div class="flex-container">
    <div class="card card-about">
        <img src="<?php echo base_url(); ?>/Assets/Resa.jpg" class="imgabt" alt="alvin" style="width:100%">
        <h2>Resa Swastyani</h2>
        <p class="title-card">Back End Developer</p>
        <p>El Rahma University</p>
    </div>
      <div class="card card-about">
        <img src="<?php echo base_url(); ?>/Assets/acha.png" class="imgabt" alt="alvin" style="width:100%">
        <h2>Athaya Fatharani</h2>
        <p class="title-card">Back End Developer</p>
        <p>El Rahma University</p>

      </div>
        <div class="card card-about">
          <img src="<?php echo base_url(); ?>/Assets/pita.png" class="imgabt" alt="alvin" style="width:100%">
          <h2>Fitri Novitasari</h2>
          <p class="title-card">Front End Developer</p>
          <p>El Rahma University</p>

        </div>
    <div class="card card-about">
        <img src="<?php echo base_url(); ?>/Assets/yoli.png" class="imgabt" alt="alvin" style="width:100%">
        <h2>Yuliana</h2>
        <p class="title-card">Front End Developer</p>
        <p>El Rahma University</p>

    </div>
</div>
<iframe src="https://maps.google.com/maps?q=El Rahma University&t=&z=13&ie=UTF8&iwloc=&output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</body>
</html>
