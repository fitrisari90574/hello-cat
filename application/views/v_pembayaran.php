<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment Receipt</title>

    <style>
        body {
            font-family: sans-serif;
            margin: 0;
            padding: 0;
        }

        .container {
            width: 350px;
            margin: 0 auto;
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 20px;
            background-color: #f5f5f5;
        }

        .header .store-logo {
            width: 100px;
            height: auto;
        }

        .header .store-name {
            font-size: 20px;
            font-weight: bold;
        }

        .order-details {
            padding: 20px;
            border: 1px solid #ccc;
        }
        .detail-link {
            color: red;
            margin-left: 10px;
        }
        .quantity {
            margin-left: 10px;
        }

        .quantity button {
            padding: 5px 10px;
            margin: 0 5px;
            border: 1px solid #ccc;
            background-color: #f5f5f5;
            cursor: pointer;
            margin-left: 20px;
        }


        .order-details h3 {
            font-size: 18px;
            margin-bottom: 10px;
        }
        .price {
            margin-left: 90px;
        }

        .order-details table {
            width: 100%;
            border-collapse: collapse;
            font-size: 14px;
        }

        .order-details table th,
        .order-details table td {
            padding: 5px;
            border: 1px solid #ccc;
            text-align: left;
        }

        .order-summary {
            padding: 20px;
            border: 1px solid #ccc;
            background-color: rgba(223, 139, 40, 0.3)
        }

        .order-summary h4 {
            font-size: 16px;
            margin-bottom: 10px;
        }

        .order-summary table {
            width: 100%;
            border-collapse: collapse;
            font-size: 14px;
        }

        .order-summary table th,
        .order-summary table td {
            padding: 5px;
            border: 1px solid #ccc;
            text-align: right;
        }

        .order-summary table .total {
            font-weight: bold;
        }
        .checklist {
            color: green;
            margin-left: 200px;
            
        }
        .payment-section {
            display: flex;
            align-items: center;
            margin-top: 50px;
        }

        .pay-button {
            margin-left: 100px;
            background-color: red;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .footer {
            padding: 20px;
            background-color: #f5f5f5;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <img src="https://i.postimg.cc/BQDjBdfK/removal-1-2x.png" alt="Store Logo" class="store-logo">
        <span class="store-name">Pembayaran Hellocat</span>
    </div>

    <div class="order-details">
    <h3>Biaya Konsultasi <span class="detail-link">Lihat Detail</span></h3>
    <p><strong>1 sesi (1x24-jam)</strong> <span class="quantity"> <button>-</button> 1 <button>+</button></span></p>
    <p> Rp 50.000</p>
    </div>

    <div class="order-details">
    <h3 style="color: red;">Detail Pembelian</h3>

    <p>1 sesi (1x24-jam) <span class="price">Rp 50.000</span></p>
    <p><strong> Total Biaya <span class="price">Rp 50.000</span></strong></p>
</div>


    <div class="order-summary">
    <h4>Metode Pembayaran</h4>
    <p>Saldo Hellocat</p>
    <p>Rp 50.000 <span class="checklist">&#10004;</span></p>
</div>

    <div class="payment-section">
    <p><strong>Rp 50.000</strong></p> <p>HelloCat</p>
    <button class="pay-button">Bayar</button>
</div>
   
<script>
    document.querySelector(".quantity button").forEach(function(button) {
  button.addEventListener("click", function() {
    const quantity = parseInt(this.parentElement.querySelector(".quantity").textContent);
    if (this.textContent === "+") {
      this.parentElement.querySelector(".quantity").textContent = String(quantity + 1);
    } else {
      if (quantity > 0) {
        this.parentElement.querySelector(".quantity").textContent = String(quantity - 1);
      }
    }
  });
});
</script>
    
