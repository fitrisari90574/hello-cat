<!DOCTYPE html>
<html>
<head>
    <title>Payment Proof</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f6dab6;
            padding: 20px;
        }

        h1 {
            font-size: 24px;
            color: #333;
        }

        h2 {
            font-size: 20px;
            margin-top: 20px;
        }

        p {
            font-size: 16px;
            margin: 5px 0;
        }

        .payment-details {
            background-color: #e29685;
            border-radius: 20px;
            padding: 20px;
        }

        /* Add more CSS styles as needed */
    </style>
</head>
<body>
    <h1>Payment Proof</h1>

    <?php if ($payment_proof): ?>
        <!-- Display payment proof details -->
        <div class="payment-details">
            <h2>Chat Information</h2>
            <p>Date and Time of Chat: <?php echo $payment_proof['time']; ?></p>
            <p>Doctor's Name: <?php echo $payment_proof['nama_dokter']; ?></p>
            <p>Price: $<?php echo $payment_proof['harga_dokter']; ?></p>
            <p>Session: <?php echo $payment_proof['session']; ?></p>
            <p>User's Name: <?php echo $payment_proof['nama_user']; ?></p>
        </div>

        <!-- You can display other payment proof details as needed -->
    <?php else: ?>
        <p>Payment proof not found or user not found.</p>
    <?php endif;
    ?>

</body>
</html>
