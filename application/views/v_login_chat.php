<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Chat</title>
    <style>
        body {
            font-family: sans-serif;
			background-image: url("asset/bg1.png");
        }

        .container {
            max-width: 960px;
            margin: 0 auto;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .login-box {
            width: 50%;
            padding: 20px;
            border: 1px solid #ccc;
			background-color: rgba(223, 147, 40, 0.3);
			margin-top: 100px;
        }

        .login-box h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        .login-box form {
            margin-top: 20px;
        }

        .login-box .btn-primary {
            width: 100%;
            margin-top: 20px;
			background-color: #F25019;
        }

        .login-image {
            width: 50%;
            padding: 20px;
        }

        .login-image img {
            width: 100%;
        }

        .login-description {
            position: relative;
            bottom: 20px;
			margin-left: 120px;
			margin-top: 50px;
			margin-bottom: 50px;
        }

        .form-group {
            margin-bottom: 20px;
        }

        .form-group label {
            margin-bottom: 5px;
        }

        .form-group input {
            width: 100%;
        }
		.deskripsi {

		bottom: 0;
		left: 50%;
		}
    </style>
</head>
<body>

<div class="container">
    <div class="login-box">
        <h1>Data Pasien</h1>
        <form action="<?php echo base_url('user/login_chat'); ?>" method="post">
            <div class="form-group">
                <label for="email">Nama Pasien</label>
                <input type="text" class="form-control" id="nama_user" name="nama_user" placeholder="Masukan nama pasien">
            </div>
            <div class="form-group">
                <label for="password">Email</label>
                <input type="password" class="form-control" id="email" name="email" placeholder="Masukan email">
            </div>
			<div class="form-group">
                <label for="password">Ras</label>
                <input type="password" class="form-control" id="ras" name="ras" placeholder="Masukan ras">
            </div>
            <button type="submit" class="btn btn-primary">Sign In</button>
        </form>
		<p class="textp">Don't Have An Account yet? <a href="<?= base_url('user/signup')?>" class="texta">Register for free</a></p>
    </div>
    <div class="login-image">
        <img src="https://i.postimg.cc/BQDjBdfK/removal-1-2x.png" alt="Doctor">
    </div>
	
</div>
	<div class="login-description">
        <h2> Memiliki Dokter yang berpengalaman</h2>
		<p> Dokter Online Halodoc bukanlah praktisi kesehatan sembarangan. Kamu bisa terhubung langsung dengan cepat dan mudah, serta tidak perlu meragukan kualitas konsultasi dan penanganan yang ditawarkan.</p>
    </div>
	<div class="login-description">
        <h2> Privasi kamu tetap terjaga</h2>
		<p> Tak perlu khawatir, semua percakapan dan diagnosis kesehatanmu dengan dokter di Halodoc akan terjaga keprivasiannya.
			Bekerja sebagai layanan kesehatan online terbaik dan terpercaya di Indonesia
			Tidak sebatas tanya dokter saja, Halodoc juga menyajikan untukmu berbagai artikel informasi kesehatan dan berita kesehatan terkini. Ada banyak kategori kesehatan untuk berita terkini, tips dan rekomendasi, hingga pengetahuan umum yang pastinya bermanfaat agar kamu bisa lebih waspada dan peduli terhadap kesehatan diri sendiri maupun keluarga tercinta.
		</p>
    </div>
	<div class="login-description">
        <h2> Cara Menghubungi Dokter Online</h2>
		<p> Konsultasi dengan dokter Halodoc secara online bisa dilakukan dengan cepat. Hanya dengan tiga langkah mudah kamu bisa terhubung dengan dokter yang kamu butuhkan. Ada dokter spesialis saraf, spesialis gigi, hingga spesialis bedah yang bisa kamu hubungi 24 jam.
		</p>
    </div>



</body>
</html>
